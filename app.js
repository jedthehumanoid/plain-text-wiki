
/**
 * Module dependencies.
 */

var express = require('express');
var wiki = require('./routes/wiki');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);

app.use(express.static(path.join(__dirname, 'public')));
app.use('/graildiary', express.static(path.join(__dirname, 'graildiary')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//app.get('/', routes.index);
//app.get('/users', user.list);

app.get('/graildiary', wiki.index);
app.get('/graildiary/*.md', wiki.markdown);
app.post('/graildiary/save.html', wiki.save);
app.post('/graildiary/preview.html', wiki.preview);
app.post('/graildiary/add.html', wiki.add);
app.post('/graildiary/remove.html', wiki.remove);
//app.post('/graildiary/markdown.html', wiki.postmarkdown);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
