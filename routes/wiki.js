var md = require( "markdown" ).markdown;
var fs = require("fs");
var url = require("url");
var macros = require("./macros");

exports.markdown = function(req, res){
    console.log("Passing markdown");
    
    var file = __dirname+'/..'+decodeURIComponent(req.path);

    if (fs.existsSync(file)) {

    fs.readFile(file, function (err, data) {
    
	if (err) {
        throw err;
	}

	var data=data.toString();
	var plaintext=data;
	macros.expand(data, function(data){
	    console.log(data);
	    var title=req.path.split('/')[req.path.split('/').length-1]
	    title=title.slice(0,-3);
	    res.render('markdown', { title: title, md:md, markdown:data, plaintext:plaintext});
	});
    });
    }

    else {
	console.log("EEEEEEEEEMPTY");
	var title=req.path.split('/')[req.path.split('/').length-1];
	title=title.slice(0,-3);
	var markdown="";
	var plaintext="";
	res.render('markdown', { title: title, md:md, markdown:"", plaintext:plaintext});
    }
};

exports.save = function(req, res){
    console.log("SAVING "+req.body.url);
    
    var file = __dirname+'/..'+decodeURIComponent(req.body.url);
    var data = req.body.data;

    fs.writeFile(file, data, function(err) {
    
	if (err) {
            throw err;
	}
	else {
	    res.send("MIU:OK");
	}
    });
};

exports.preview = function(req, res) {
    console.log("Previewing");
    res.render('preview', { title: req.path.split('/').slice(-1), md:md, markdown:req.body.data});
};

exports.add = function(req, res) {
    console.log("ADDING "+req.body.url);
    
    var file = __dirname+'/../graildiary/'+decodeURIComponent(req.body.url);
    console.log(file);
    
    fs.writeFile(file, "asf", function(err) {
    
	if (err) {
            throw err;
	}
	else {
	    res.send("OK");
	}
    });

//    res.render('preview', { title: req.path.split('/').slice(-1), md:md, markdown:req.body.data});
};

exports.remove = function(req, res) {
    console.log("REMOVING "+req.body.url);
    
    var file = __dirname+'/..'+decodeURIComponent(req.body.url);
    console.log(file);
    
    fs.unlink(file, function(err) {
    
	if (err) {
            throw err;
	}
	else {
	    res.send("OK");
	}
    });

};

exports.index = function(req, res) {
    res.redirect("graildiary/index.md");
};
