var fs = require("fs");
var findit = require('findit');

var allfiles = exports.allfiles = function(callback){
    var path=__dirname.substr(0,__dirname.lastIndexOf('/'))+"/graildiary/";

    var finder = findit(path);
    var files="";

    finder.on('file', function (file, stat) {
	file = file.substr(path.length);
	if (file.lastIndexOf('.md') == file.length-3) {
	    files=files+"["+file+"]("+file+")  \n";
	}
    });    

    finder.on('end', function () {
	callback(files);
    });
};

exports.expand = function(text, callback){
    console.log("Expanding macros!");

    if(text.indexOf("{{allfiles}}")>-1) {
	allfiles(function(files){
	    text=text.replace("{{allfiles}}", files);
	    callback(text);
	});
    }
    else {
	callback(text);
       }
}
