A pretty simple wiki
------------------

Basically the idea is:
Plain text files just placed in a folder along with other files, like images.  
Simple markup language (in this case [Markdown](http://daringfireball.net/projects/markdown/)) enable things 
like linking between files and putting images in texts. 

Markup files is then just parsed and served as html.

Other similar projects that i didn't necessarily know about starting this:  
[Githubs Gollum](https://github.com/gollum/gollum), [blogpost](https://github.com/blog/699-making-github-more-open-git-backed-wikis)  
[Commonplace](http://helloform.com/projects/commonplace/), [github](https://github.com/fredoliveira/commonplace)    
[markdoc](https://github.com/zacharyvoase/markdoc), [blog](http://zacharyvoase.com/2009/11/17/markdoc/), seems to be abanonded  

Markup
------

![example](http://cdn.bitbucket.org/jedthehumanoid/plain-text-wiki/downloads/example.png)

This has some shortcomings:

- Edit functionality through web-interface would be smooth
- Some simple macro-expansion could be good to minimize manual editing, i.e. list all files in directory et.c.

Extra stuff
----------

Awesome jQuery editor [markItUp](http://markitup.jaysalvat.com/) provides editing.

![markitup](http://cdn.bitbucket.org/jedthehumanoid/plain-text-wiki/downloads/markitup.png)

Macro expansion could be done in whatever backend is running server side. In this case [node.js](http://nodejs.org/) which 
does some simple string replacement before sending to markdown parser.
Something like for example **{allfiles}** would expand in place to:  

[todo.md](todo.md)  
[lego.md](lego.md)  
[Projects/Treehouse.md](Projects/Treehouse.md)  
[Food/Guacamole.md](Food/Guacamole.md)  
[Food/ice cream.md](Food/ice cream.md)  


Thats it!
--------

This is really simple but actually covers my needs for a simple self hosted wiki.

Interesting related things
-----------------------

Adam Savages discusses his [project repository](http://www.ted.com/talks/adam_savage_s_obsessions.html) 

